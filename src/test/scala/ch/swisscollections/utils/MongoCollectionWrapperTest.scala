/*
 * Mongo Extractor
 * Copyright (C) 2020 UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.swisscollections.utils


import ch.memobase.settings.SettingsLoader
import ch.swisscollections.App.deserialize
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers
import ch.swisscollections.utils.MongoCollectionWrapper

import scala.collection.JavaConverters._



class MongoCollectionWrapperTest extends AnyFunSuite with Matchers {
  //needs a mongo connection to test this. To test locally, make a tunnel to dd-db6
  ignore("mongo one record") {

    val settings = new SettingsLoader(List(
      "mongoDb",
      "mongoCollection",
      "mongoUri"
    ).asJava,
      "test.yml", //or local.yml to load local settings
      true,
      false,
      false,
      false)

    val collectionWrapper = MongoCollectionWrapper(settings)

    val records = collectionWrapper.findById("(UBS)oai:alma.41SLSP_UBS:9972623462005504")

    assert(records.iterator.length == 1)

    assert(records.head.get("_id") == "(UBS)oai:alma.41SLSP_UBS:9972623462005504")


  }
}

