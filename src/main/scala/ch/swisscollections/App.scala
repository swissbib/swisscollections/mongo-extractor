/*
 * Mongo Extractor
 * Copyright (C) 2020 UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.swisscollections


import java.io.ByteArrayOutputStream
import java.util.zip.Inflater

import org.apache.logging.log4j.scala.Logging
import utils.MongoCollectionWrapper
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import ch.memobase.settings.SettingsLoader

import scala.collection.JavaConverters._
import scala.xml.XML


object App extends scala.App with Logging {

  val settings = new SettingsLoader(List(
    "mongoDb",
    "mongoCollection",
    "mongoUri"
  ).asJava,
    "app.yml", //or local.yml to load local settings
    true,
    false,
    false,
    false)

  val collectionWrapper = MongoCollectionWrapper(settings)

  val kafkaProps = settings.getKafkaProducerSettings

  val kp = new KafkaProducer[String, String](kafkaProps)

  //if RECORD_ID is set we fetch only this record, otherwise we fetch all records
  val records = sys.env.get("RECORD_ID") match {
      case Some(id) => collectionWrapper.findById(id)
      case None => collectionWrapper.findAllDocs()
    }

  val cursor = records.iterator

  while (cursor.hasNext) {
    val dbObject = cursor.next()
    val doc = dbObject.get("record").asInstanceOf[org.bson.types.Binary]
    val record = deserialize(doc.getData)
    val id = dbObject.get("_id").asInstanceOf[String]

    kp.send(new ProducerRecord[String, String](settings.getOutputTopic, id, record))
  }

  kp.close()

  System.exit(0)


  def deserialize(data: Array[Byte]): String = {
    val decompressor = new Inflater()
    decompressor.setInput(data)
    val bos = new ByteArrayOutputStream(data.length)
    val buffer = new Array[Byte](8192)
    while (!decompressor.finished) {

      val size = decompressor.inflate(buffer)
      bos.write(buffer, 0, size)
    }
    bos.toString


  }
}


