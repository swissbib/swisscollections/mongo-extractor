/*
 * Mongo Extractor
 * Copyright (C) 2020 UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.swisscollections.utils

import com.mongodb.{BasicDBObject, MongoClient, MongoClientURI}
import com.mongodb.client.MongoCollection
import org.bson.Document
import ch.memobase.settings.SettingsLoader
import org.bson.conversions.Bson

import scala.collection.JavaConverters.iterableAsScalaIterableConverter

class MongoCollectionWrapper(val collection: MongoCollection[Document]) {

  def findAllDocs():Iterable[Document] = {
    collection.find().asScala
  }

  def findById(identifier:String): Iterable[Document] = {
    val bdbo = new BasicDBObject()
    bdbo.put("_id",  identifier)
    collection.find(bdbo).asScala
  }

}

object MongoCollectionWrapper {

  def apply (settings: SettingsLoader): MongoCollectionWrapper = {

    val uri = new MongoClientURI(settings.getAppSettings.getProperty("mongoUri"))
    val mongoClient = new MongoClient(uri)

    val database = mongoClient.getDatabase(settings.getAppSettings.getProperty("mongoDb"))
    val collection = database.getCollection(settings.getAppSettings.getProperty("mongoCollection"))

    new MongoCollectionWrapper(collection)
  }

}
