# Mongo Extractor

Given a Mongo Collection, this microservice takes all documents from a collection and write them in Kafka. 

See https://rephelv.atlassian.net/wiki/spaces/RH/pages/120553497/Mongo+Extractor for more.

## How to run on Kubernetes

First define in `k8s-manifests/prod/mongo-extractor-prod-job.yml`, the mongo collection you want to dump to kafka. Then

```
cd k8s-manifests
cd prod
kubectl apply -f mongo-extractor-prod-job.yml
```

You might need to do that before :
```
kubectl delete -f mongo-extractor-prod-job.yml
```
if the job is already running.

Don't forget to create the output topic in Kafka if needed.

## Reload IZ Bern / Zurich / Basel / Luzern

```
cd k8s-manifests
cd prod
kubectl delete -R -f IZ-specific/
kubectl apply -R -f IZ-specific/
```
## Reload only one IZ

```
cd k8s-manifests
cd prod
kubectl delete -f IZ-specific/job-basel.yml
kubectl apply -f IZ-specific/job-basel.yml
```
## How to fetch only one specific record from mongo

Insert the IZ ID of the record in the environment variable `RECORD_ID`. See `k8s-manifests/one-record/job-basel-one-record.yml` for an example

Run the job : 

```
kubectl apply -f k8s-manifests/test/one-record/job-basel-one-record.yml
```
This will fetch this record from mongo and write it to the relevant kafka topic. This might be useful for debugging purposes (finding which microservice is problematic).

Be careful : the solr indexer needs at least 500 records to do some indexing.



## How to run locally

If needed : do a tunnel to mongo

```
ssh -L 29017:sb-udb10.swissbib.unibas.ch:29017 swissbib@sb-udb10.swissbib.unibas.ch
```

Start you local kafka server
```
./bin/zookeeper-server-start.sh config/zookeeper.properties
./bin/kafka-server-start.sh config/server.properties
```

Adapt `local.yml` to point to your local mongo and kafka

Replace the string "app.yml" by "local.yml" in App.scala 

In IntelliJ, run it using the small green triangle next "object App" in App.scala


## Set mongo URI in kubernetes secret

kubectl create secret generic mongo-server-dd-db6\
   --from-literal=MONGO_URI=mongodb://admin:jqertzUH67Bvd@dd-db6.ub.unibas.ch:29017/?authSource=admin
