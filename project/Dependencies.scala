/*
 * Mongo Extractor
 * Copyright (C) 2020  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import sbt._

object Dependencies {
  lazy val kafkaV = "2.3.1"
  lazy val log4jV = "2.17.0"
  lazy val scalatestV = "3.1.2"
  lazy val mongoversion = "3.12.7"
  lazy val xmlversion = "2.3.0"

  lazy val mongoclient = "org.mongodb" % "mongo-java-driver" % mongoversion
  lazy val kafkaClients = "org.apache.kafka" % "kafka-clients" % kafkaV
  lazy val log4jCore = "org.apache.logging.log4j" % "log4j-core" % log4jV
  lazy val log4jScala = "org.apache.logging.log4j" %% "log4j-api-scala" % "12.0"
  lazy val log4jSlf4j = "org.apache.logging.log4j" % "log4j-slf4j-impl" % log4jV
  lazy val scalaTest = "org.scalatest" %% "scalatest" % scalatestV
  lazy val kafkaMetadataWrapper = "org.swissbib" % "kafka-metadata-wrapper" % "2.4.0"
  lazy val scalaxml = "org.scala-lang.modules" %% "scala-xml" % xmlversion
  lazy val memobaseServiceUtils = "org.memobase" % "memobase-service-utilities" % "3.0.1"
}
